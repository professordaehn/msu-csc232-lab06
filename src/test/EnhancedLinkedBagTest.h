/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file   EnhancedArrayBagTest.h
 * @author Jim Daehn <jdaehn@missouristate.edu>
 * @brief  Specification of EnhancedArrayBag Unit Test.
 * @see    http://sourceforge.net/projects/cppunit/files
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#ifndef ENHANCED_ARRAY_BAG_TEST_H__
#define ENHANCED_ARRAY_BAG_TEST_H__

#include <cppunit/extensions/HelperMacros.h>
#include <cstdlib>
#include <string>
#include <vector>
#include "../main/EnhancedLinkedBag.h"

/**
 * @brief  Specification of EnhancedArrayBag Unit Test.
 * @remark DO NOT MODIFY THE SPECIFICATION OR IMPLEMENTATION OF THIS CLASS! ANY
 *         MODIFICATION TO THIS CLASS WILL RESULT IN A GRADE OF 0 FOR THIS LAB!
 */
class EnhancedLinkedBagTest : public CPPUNIT_NS::TestFixture {
CPPUNIT_TEST_SUITE(EnhancedLinkedBagTest);

        CPPUNIT_TEST(testUnionResults);
        CPPUNIT_TEST(testUnionResultSize);
        CPPUNIT_TEST(testIntersectionResults);
        CPPUNIT_TEST(testIntersectionResultSize);
        CPPUNIT_TEST(testDifferenceResults);
        CPPUNIT_TEST(testDifferenceResultSize);

    CPPUNIT_TEST_SUITE_END();

public:
    /**
     * Default constructor.
     */
    EnhancedLinkedBagTest();

    /**
     * EnhancedArrayBagTest destructor.
     */
    virtual ~EnhancedLinkedBagTest();

    /**
     * Method that executes before each individual test.
     */
    void setUp();

    /**
     * Method that executes after each individual test.
     */
    void tearDown();
private:
    static const int EXPECTED_UNION_SIZE;
    static const int EXPECTED_INTERSECTION_SIZE;
    static const int EXPECTED_DIFFERENCE_SIZE;

    static const std::vector<std::string> lhdata;
    static const std::vector<std::string> rhdata;
    static const std::vector<std::string> expectedUnion;
    static const std::vector<std::string> expectedIntersection;
    static const std::vector<std::string> expectedDifference;
    EnhancedLinkedBag<std::string> lhbag;
    EnhancedLinkedBag<std::string> rhbag;

    void testUnionResults();
    void testUnionResultSize();
    void testIntersectionResults();
    void testIntersectionResultSize();
    void testDifferenceResults();
    void testDifferenceResultSize();
};

#endif /* ENHANCED_ARRAY_BAG_TEST_H__ */
